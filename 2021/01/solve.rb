#!/bin/ruby
#file = File.open("input_ex.txt", "rb")

def old
  file = File.open("input.txt", "rb")
  contents = file.read.split
  last=nil
  rep=0
  contents.map(&:to_i).each{|n|
    if last.nil?
      last=n    
    elsif n>last
      rep+=1
      last=n
    else
      last=n
    end
  }
  p rep
  #1832
end



## Part 02
require_relative '../../framework.rb'

def solve(input)
  last=nil
  rep=0
  input.each_cons(3){|a|
    n=a.map(&:to_i).sum
    if last.nil?
      last=n    
    elsif n>last
      rep+=1
      last=n
    else
      last=n
    end    
  }
  p rep
end

def run
  a=finput 'input_02.txt'
  p solve(a)
end

def test
  m=finput('fixture_02.txt')
  assert(solve(m)==5, "Sum is 5")
end


#
test
run
