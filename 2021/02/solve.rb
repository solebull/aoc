#!/bin/ruby
#

## Part 01
require_relative '../../framework.rb'

def solve(input)
  rep=0
  position=depth=0

  input.each{|i|
    s=i.split
    val=s[1].to_i
    #STDERR.puts s,val, i[0][0]
    case i[0][0]
    when 'f'
      position+=val
    when 'd'
      depth+=val
    when 'u'
      depth-=val
      depth=0 if depth<0
    end
  }

  STDERR.puts "forward #{position}"
  STDERR.puts "depth #{depth}"
  p position* depth
  return position*depth
end

def run
  a=finput 'input_01.txt'
  p solve(a)
end

def test
  m=finput('fixture_01.txt')
  assert(solve(m)==150, "Sum is 5")
end

## Part 02
require_relative '../../framework.rb'

def solve_02(input)
  rep=0
  position=depth=aim=0

  input.each{|i|
    s=i.split
    val=s[1].to_i
    #STDERR.puts s,val, i[0][0]
    case i[0][0]
    when 'f'
      position+=val
      depth+=aim*val
#      depth=0 if depth<0
    when 'd'
      aim+=val
    when 'u'
      aim-=val
    end
  }

  STDERR.puts "forward #{position}"
  STDERR.puts "depth #{depth}"
  STDERR.puts "aim #{aim}"
  p position* depth
  return position*depth
end

def run_02
  a=finput 'input_02.txt'
  p solve_02(a)
end

def test_02
  m=finput('fixture_01.txt')
  assert(solve_02(m)==150, "Sum is 5")
end


#
test_02
run_02


#
#test
#run


