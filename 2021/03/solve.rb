#!/bin/ruby
# coding: utf-8

# Problem is https://adventofcode.com/2021/day/3

# Part 01
require_relative '../../framework.rb'

# Get concatenated input for the given index's column
def pos(input,index)
  a=[]
  input.each{|i|
    a<< i[index]
  }
  return a
end

def solve(input)
  gamma=epsilon=0

  n1=""
  n2=""
  input[0].size.times{|i|
    t=pos(input, i).tally
    b=t.sort_by{|_,count| -count}
    n1+=b[0][0]
    n2+=t.sort_by{|_,count| count}[0][0]
  }
  gamma=n1.to_i(2)
  epsilon=n2.to_i(2)
  
  return gamma*epsilon
end

def run
  a=finput 'input_01.txt'
  puts "Result 01 : " + solve(a).to_s
end

def test
  print("Test 01 ...")
  m=finput('fixture_01.txt')
  v=solve(m)
  assert(v==198, "Gamma x Epsilon")
  puts("Ok")
end

#
test
run

# Part 02

def solve_02(input)
  oxygen=co2=0

  inp=input
  idx=0
  while inp.size>1
    t=pos(inp, idx).tally
    b=t.sort_by{|_,count| -count}[0][0]
#    p b
    inp=inp.select{|e| e[idx]==b}
#    p inp
    idx+=1
  end
  
  return oxygen*co2
end

def run_02
  a=finput 'input_01.txt'
  p solve_02(a)
end

def test_02
  print "Test 02 ..."
  m=finput('fixture_01.txt')
  v=solve_02(m)
  assert(v==198, "Gamma x Epsilon")
  puts "Ok"
end

test_02
run_02
