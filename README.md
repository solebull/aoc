# aoc

**This project may be dead** (I'm not streaming dev anymore).

*Advent Of Code*-related project. Authentication is done via solebull's
twitter profile.

https://adventofcode.com/2021/

# Framework

The `framework.rb` file in the root directory if a standard way to Handle
file input, fixture files or assert.
