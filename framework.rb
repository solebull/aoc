# AOC implentation and testing framework

# Read the given file AS TEXT!
def finput(filename='./input.txt')
  file = File.open(filename)
  content=file.read.split "\n"
  file.close()
  return content
end

# Raise if condition is false
def assert(text, cond)  
  raise "'#{text}' is false" if !cond
end
  
