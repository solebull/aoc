#!/bin/ruby

require_relative '../framework.rb'

def solve(input)
  input.map(&:to_i).combination(2).to_a.each{|a,b|
    return a*b if a+b==2020
  } 
end

def run
  a=finput
  p solve(a)
end

def test
  m=finput('fixture_01.txt')
  
  assert("testing sum", solve(m)==514579)
end


#
test
run
